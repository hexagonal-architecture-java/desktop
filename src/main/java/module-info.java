module account.desktop {
    // use system
    requires account.system;

    // use spring
    requires javax.inject;
    requires spring.tx;
    requires spring.core;
    requires spring.beans;
    requires spring.context;
    requires java.sql;

    // use javafx
    requires javafx.controls;

    //open builds and screens
    opens account.screen;
    opens account.dev;
//  opens account.hml;
//  opens account.prd;
}