package account.dev;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({
        //frontend adapters javafx
        "account.dev",
        //system objects
        "account.system",
        //fake adapters
        "account.adapter"})
public class Build2 {
    // Build 2: Javafx Adapter -> System <- Mock Adapters
}