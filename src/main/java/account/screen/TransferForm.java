package account.screen;

import account.system.domain.model.Account;
import account.system.usecase.port.TransferPort;

import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;

import static java.util.Objects.isNull;

// This class is responsible for the transfer screen using JavaFx.
@Named
public class TransferForm {

    private TextField tfDebit;
    private TextField tfNameDebit;
    private TextField tfCredit;
    private TextField tfNameCredit;
    private TextField tfValue;
    private TransferPort port;

    @Inject
    public TransferForm(TransferPort port) {
        this.port = port;
    }

    private void clean() {
        tfDebit.setText("");
        tfNameDebit.setText("");
        tfCredit.setText("");
        tfNameCredit.setText("");
        tfValue.setText("");
    }

    private Integer get(String value) {
        try {
            return Integer.valueOf(value);
        } catch (Exception e) {
            return null;
        }
    }

    private void message(String text) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Bank transfer");
        alert.setHeaderText(null);
        alert.setContentText(text);
        alert.showAndWait();
    }

    private void get(TextField tfInput, TextField tfOutput) {
        try {
            Account account = this.port.getAccount(get(tfInput.getText()));
            if (isNull(account)) {
                tfOutput.setText("");
            } else {
                tfOutput.setText(account.getAccountHolder() + " - Saldo R$ " + account.getBankBalance());
            }
        } catch (Exception e) {
            message(e.getMessage());
        }
    }

    private BigDecimal get() {
        try {
            return new BigDecimal(tfValue.getText());
        } catch (Exception e) {
            return null;
        }
    }

    private FlowPane mountScreen() {
        FlowPane pn = new FlowPane();
        pn.setHgap(10);
        pn.setVgap(10);

        pn.getChildren().add(new Label(" Debit Account:"));
        tfDebit = new TextField();
        tfDebit.setPrefWidth(50);
        tfDebit.focusedProperty().addListener((o, v, n) -> {
            if (!n) get(tfDebit, tfNameDebit);
        });

        pn.getChildren().add(tfDebit);

        tfNameDebit = new TextField();
        tfNameDebit.setPrefWidth(300);
        tfNameDebit.setEditable(false);
        pn.getChildren().add(tfNameDebit);

        pn.getChildren().add(new Label(" Credit Account:"));
        tfCredit = new TextField();
        tfCredit.setPrefWidth(50);
        tfCredit.focusedProperty().addListener((o, v, n) -> {
            if (!n) get(tfCredit, tfNameCredit);
        });
        pn.getChildren().add(tfCredit);

        tfNameCredit = new TextField();
        tfNameCredit.setEditable(false);
        tfNameCredit.setPrefWidth(300);
        pn.getChildren().add(tfNameCredit);

        pn.getChildren().add(new Label(" Valor R$:"));
        tfValue = new TextField();
        tfValue.setPrefWidth(200);
        pn.getChildren().add(tfValue);

        Button bt = new Button();
        bt.setOnAction((ev) -> {
            try {
                this.port.transfer(get(tfDebit.getText()), get(tfCredit.getText()), get());
                clean();
                message("transfer was successfully done!");
            } catch (Exception e) {
                message(e.getMessage());
            }
        });
        bt.setText("transfer");
        pn.getChildren().add(bt);
        return pn;
    }

    public void show(Stage stage) {
        stage.setTitle("JavaFX Adapter");
        Scene scene = new Scene(mountScreen(), 500, 100);
        stage.setScene(scene);
        stage.show();
    }
}
