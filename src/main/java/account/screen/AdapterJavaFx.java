package account.screen;

import account.dev.Build2;
import javafx.application.Application;
import javafx.stage.Stage;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

// Responsible by the initial execution
public class AdapterJavaFx extends Application {

    private ApplicationContext spring;

    @Override
    public void init() {
        System.out.println("Initializing spring..");
        spring = new AnnotationConfigApplicationContext(Build2.class);
        //spring = new AnnotationConfigApplicationContext(Build3.class);
        //spring = new AnnotationConfigApplicationContext(Build4.class);
    }

    @Override
    public void start(Stage stage) {
        TransferForm form = spring.getBean(TransferForm.class);
        form.show(stage);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
